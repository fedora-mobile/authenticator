%global commit 60f1ed5fa8bebb93a10771eae021df4e7936b030

Name:           authenticator
Version:        3.32.2+git20200318
Release:        1%{?dist}
Summary:        Two factor authentication app

License:        GPLv3+
URL:            https://gitlab.gnome.org/World/Authenticator/
Source0:        https://gitlab.gnome.org/World/Authenticator/-/archive/%{commit}/Authenticator-%{commit}.tar.gz

BuildRequires:  gcc
BuildRequires:  meson
BuildRequires:  python3
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gobject-2.0)
BuildRequires:  pkgconfig(gobject-introspection-1.0)
BuildRequires:  pkgconfig(libsecret-1)
BuildRequires:  pkgconfig(gtk+-3.0) >= 3.16
BuildRequires:  pkgconfig(zbar) >= 0.20.1
BuildRequires:  glib2
BuildRequires:  gtk-update-icon-cache
BuildRequires:  desktop-file-utils

%description
Two-factor authentication application built for GNOME.

%global debug_package %{nil}

%prep
%autosetup -p1 -n Authenticator-%{commit}


%build
%meson
%meson_build


%install
%meson_install


%check
%meson_test


%files
%{_bindir}/authenticator
%{_prefix}/lib/python3.8/site-packages/Authenticator/
%{_libexecdir}/authenticator-search-provider
%{_datarootdir}/applications/com.github.bilelmoussaoui.Authenticator.desktop
%{_datarootdir}/com.github.bilelmoussaoui.Authenticator/com.github.bilelmoussaoui.Authenticator.gresource
%{_datarootdir}/glib-2.0/schemas/com.github.bilelmoussaoui.Authenticator.gschema.xml
%{_datarootdir}/gnome-shell/search-providers/com.github.bilelmoussaoui.Authenticator.SearchProvider.ini
%{_datarootdir}/gnome-shell/search-providers/com.github.bilelmoussaoui.Authenticator.SearchProvider.service
%{_datarootdir}/icons/hicolor/scalable/apps/com.github.bilelmoussaoui.Authenticator.svg
%{_datarootdir}/icons/hicolor/symbolic/apps/com.github.bilelmoussaoui.Authenticator-symbolic.svg
%{_datarootdir}/locale/
%{_datarootdir}/metainfo/com.github.bilelmoussaoui.Authenticator.appdata.xml


%changelog
* Wed Mar 18 2020 Nikhil Jha <hi@nikhiljha.com> - 3.32.2+git20200318
- Initial packaging
